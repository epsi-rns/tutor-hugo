+++
type   = "post"
title  = "Everyday I Ask"
date   = 2015-10-03T08:08:15+07:00
slug   = "daily-wish"
tags   = ["wishful", "story"]
categories = ["love"]
+++

You know, some people makes mistakes,
and have to bear the consequences, for the rest of their life.
I made, one good decision when I was far younger than you are.
Later I know it was the right decision.
But I also have to bear the consquences, for the rest of my life.

> I wish you a good family, and happiness.
