+++
type       = "post"
title      = "Julian Baker - Something"
date       = 2018-09-07T07:35:05+07:00
categories = ["lyric"]
tags       = ["indie", "2010s"]
slug       = "julian-baker-something"
author     = "epsi"
+++

I knew I was wasting my time.
Keep myself awake at night.
'Cause whenever I close my eyes.
I'm chasing your tail lights.
In the dark.
In the dark.
In the dark.

{{< highlight haskell >}}
-- Layout Hook
commonLayout = renamed [Replace "common"]
    $ avoidStruts 
    $ gaps [(U,5), (D,5)] 
    $ spacing 10
    $ Tall 1 (5/100) (1/3)
{{< / highlight >}}

Watched you said nothing, said nothing, said nothing.
I can't think of anyone, anyone else.
I can't think of anyone, anyone else.
I can't think of anyone, anyone else.
I won't think of anyone else.
