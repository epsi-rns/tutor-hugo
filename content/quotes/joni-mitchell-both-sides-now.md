+++
type       = "post"
title      = "Joni Mitchell - Both Sides Now"
date       = 2014-01-15T07:35:05+07:00
categories = ["lyric"]
tags       = ["pop", "70s"]
slug       = "joni-mitchell-both-sides-now"
author     = "epsi"
+++

I've looked at life from both sides now.
From win and lose and still somehow.

```haskell
-- Layout Hook
commonLayout = renamed [Replace "common"]
    $ avoidStruts 
    $ gaps [(U,5), (D,5)] 
    $ spacing 10
    $ Tall 1 (5/100) (1/3)
```

It's life's illusions I recall.
I really don't know life at all.
