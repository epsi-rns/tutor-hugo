+++
type       = "post"
title      = "Bruce Springsteen - One Step Up"
date       = 2014-03-25T07:35:05+07:00
categories = ["lyric"]
tags       = ["springsteen", "80s"]
slug       = "bruce-springsteen-one-step-up"
author     = "epsi"

toc        = "toc-2014-03-springsteen"

related_link_ids = [
  "14031535"  # The River
]

+++

When I look at myself I don't see.
The man I wanted to be.
Somewhere along the line I slipped off track.
I'm caught movin' one step up and two steps back.

{{< advert src="oto-spies-01.png" >}}

There's a girl across the bar.
I get the message she's sendin'.
Mmm she ain't lookin' too married.
And me well honey I'm pretending.

{{< advert src="oto-spies-02.png" >}}

Last night I dreamed I held you in my arms.
The music was never-ending.
We danced as the evening sky faded to black.
One step up and two steps back.
