+++
type       = "post"
title      = "John Mayer - Slow Dancing in a Burning Room"
date       = 2018-02-15T07:35:05+07:00
categories = ["lyric"]
tags       = ["rock", "2010s"]
slug       = "john-mayer-slow-dancing-in-a-burning-room"
author     = "epsi"

[opengraph]
  image    = "assets/posts/2018/kiddo-007.jpg"
+++

We're going down.
And you can see it, too.

We're going down.
And you know that we're doomed.

My dear, we're slow dancing in a burning room.

![A Thinking Kiddo][image-kiddo]

-- -- --

### Example Dotfiles

ViM RC: [bandithijo][link-dotfiles]

-- -- --

### What's next ?

Our next song would be [Julian Baker][local-whats-next]

[//]: <> ( -- -- -- links below -- -- -- )

[image-kiddo]:      {{< baseurl >}}assets/posts/2018/kiddo-007.jpg

[local-whats-next]: {{< baseurl >}}quotes/2018/09/07/julian-baker-something/
[link-dotfiles]:    {{< dotfiles >}}/terminal/vimrc/vimrc.bandithijo
