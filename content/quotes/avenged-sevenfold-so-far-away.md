+++
type       = "post"
title      = "Avenged Sevenfold - So Far Away"
date       = 2018-01-15T07:35:05+07:00
categories = ["lyric"]
tags       = ["rock", "2010s"]
slug       = "avenged-sevenfold-so-far-away"
author     = "epsi"
+++

I love you, you were ready,
the pain is strong and urges rise.

But I'll see you when He let's me.
Your pain is gone, your hands untied.
