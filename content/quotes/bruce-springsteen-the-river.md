+++
type       = "post"
title      = "Bruce Springsteen - The River"
date       = 2014-03-15T07:35:05+07:00
categories = ["lyric"]
tags       = ["springsteen", "80s"]
slug       = "bruce-springsteen-the-river"
author     = "epsi"

toc        = "toc-2014-03-springsteen"

related_link_ids = [
  "14032535"  # One Step Up
]

+++

Is a dream a lie 
if it don't come true? 
Or is it something worse?
